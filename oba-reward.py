__author__ = "joecool890"
#!/usr/bin/env python2
# OBA experiment with Reward
from psychopy import core, data, event, visual, misc
import os, datetime as dt
import numpy as np
import socket
import random, collections

# --- Experiment Specifications ---
# RAs, edit + double check before running#
parNo       = 1
parAge      = 29
parGen      = 1 # 1 for Male, 2 for Female
parHand     = 1 # 1 for Right, 2 for Left

# -- Only change when necessary
prac_repeat = 0 # 1 if skipping practice
paid_exp    = 0 # 1 if paid

#  --- Experiment Variables ---
expVer      = "exp04"
exp_ver     = 4 # for data file
exp_iter    = 1 # Experiment iteration

#   Exp 2: 25-25-25-25
#   iter 1: 75% reward on w/d
#   iter 2: 100% reward
#   iter 3: 100% reward, SOA = 450
#   iter 4: 100% reward, no penalty

if socket.gethostname() == "CCAPSY107SHOM02":
    roomName    = 1
elif socket.gethostname() == "CCAPSY107SHOM01":
    roomName    = 3
elif socket.gethostname() == "Josephs-MBP-13.local":
    roomName    = 0
monitor_name    = "testMonitor"
send_email      = 1
screenVar       = 1
blockSize       = 64
reps            = 10
total           = 0 # total points start at 0

# --- Set Duration (ms) ---
objectTime  = 1
cueTime     = .1
firstISI    = .35
searchTime  = .06
secondISI   = .1
ITI         = 1

# --- Set Visual Angle ---
fixSize     = .5
targSize    = 1
cueSize     = 2
maskSize    = 2

# --- Experiment Paths and Date---
# change directory if on windows
if  os.name == "nt":
    os.chdir("C:\Users\shomsteinlab\PycharmProjects\joenah\oba-reward")
    monitor_name = "labComputer"

expName         = "oba-reward"
filePath        = "./data/"
stimPath        = "./stim/"
rootBalancePath = "./balanceFactors/"

# --- Experiment BalanceFactors file Path ---
# Exp01: 25-25-25-25 & reward random
# Exp02: 25-25-25-25 & reward biased to wthn/discard
# Exp03: 25-25-25-25 & reward biased to btwn/discard(?)

balancePath = rootBalancePath + expVer + "/"
date        = dt.datetime.now().strftime("%Y-%m-%d_%H-%M-%S")  # get date as YMD_H_M_S

# --- Test Path Directory ---
try:
    os.makedirs(filePath)
except OSError:
    print "directory " + filePath + " already exists"

# --- Set Object Orientation ---
if np.mod(parNo,2) == 1:
    ori = 1 # Vertical
elif np.mod(parNo,2) == 0:
    ori = 2 # vertical

# -- Set Virtual Window ---
win = visual.Window(
    [1280, 1024],
    monitor = monitor_name,
    screen = 1,
    units = "deg",
    color = "grey",
    fullscr = screenVar
)

# --- set up fixation, cue, targets, and distractors ---
fix = visual.ImageStim(win, image = stimPath + "oba-reward_fixation.png", size = fixSize, pos = [0, 0], units = "deg")

cue = visual.ImageStim(win, image = stimPath + "oba-reward_cue.png", size = cueSize, units = "deg")

targ_t = visual.ImageStim(win, image = stimPath + "oba-reward_T.png", size = targSize,units = "deg")
targ_l = visual.ImageStim(win, image = stimPath + "oba-reward_L.png", size = targSize, units = "deg")

distractor1 = visual.ImageStim(win, image = stimPath + "oba-reward_D.png", size = targSize, units = "deg")
distractor2 = visual.ImageStim(win, image = stimPath + "oba-reward_D.png", size = targSize, units = "deg")
distractor3 = visual.ImageStim(win, image = stimPath + "oba-reward_D.png", size = targSize, units = "deg")

mask1 = visual.ImageStim(win, image = stimPath + "oba-reward_mask.png", size = maskSize, units = "deg", pos = (-2,2))
mask2 = visual.ImageStim(win, image = stimPath + "oba-reward_mask.png", size = maskSize, units = "deg", pos = (2,2))
mask3 = visual.ImageStim(win, image = stimPath + "oba-reward_mask.png", size = maskSize, units = "deg", pos = (-2,-2))
mask4 = visual.ImageStim(win, image = stimPath + "oba-reward_mask.png", size = maskSize, units = "deg", pos = (2,-2))

# --- set up objects ---
if ori == 1: # Vertical
    rect1 = visual.Rect(win=win, units="deg", width=2, height=6, pos=(2, 0), lineColor="black")
    rect2 = visual.Rect(win=win, units="deg", width=2, height=6, pos=(-2, 0), lineColor="black")
elif ori == 2: # Horizontal
    rect1 = visual.Rect(win=win, units="deg", width=6, height=2, pos=(0, 2), lineColor="black")
    rect2 = visual.Rect(win=win, units="deg", width=6, height=2, pos=(0, -2), lineColor="black")

# --- locations and orientations ---
locations       = ([-2,2],[2,2],[-2,-2],[2,-2])
orientations    = [0,90,180,270]

# --- hide mouse ---
event.Mouse(visible = False)

# --- Data setup ---
dataMatrix  = {}
if parNo < 10:
    rawData = filePath + "00" + str(parNo) + "_" + date + "_" + expName + "-" + expVer
    rawName = "00" + str(parNo) + "_" + date + "_" + expName + "-" + expVer
elif parNo >= 10:
    rawData = filePath + "0" + str(parNo) + "_" + date + "_" + expName + "-" + expVer
    rawName = "0" + str(parNo) + "_" + date + "_" + expName + "-" + expVer
elif parNo >= 100:
    rawData = filePath + str(parNo) + "_" + date + "_" + expName + "-" + expVer
    rawName = str(parNo) + "_" + date + "_" + expName + "-" + expVer

# --- File for balancing factors based on orientation ---
if ori == 1:
    pracPath    = rootBalancePath + "/balanceFactors-pract_vert.csv"
    importPath  = balancePath + "/balanceFactors-vert-" + expVer + ".csv"
elif ori == 2:
    pracPath    = rootBalancePath + "/balanceFactors-pract_hori.csv"
    importPath  = balancePath + "/balanceFactors-hori-" + expVer + ".csv"

# --- Define conditions and create randomizing trial handler ---
if prac_repeat == 0:
    pracMatrix  = data.importConditions(pracPath)
    practice = data.TrialHandler(
        trialList=pracMatrix,
        nReps=1,
        method="random"
    )

trialMatrix = data.importConditions(importPath)
trials      = data.TrialHandler(
    trialList = trialMatrix,
    nReps = reps,
    method = "random"
)

# add trials to the experiment handler to store data
currExp = data.ExperimentHandler(
    name = "oba-reward",
    version = "1.0",
    extraInfo = dataMatrix,
    saveWideText = True,
    dataFileName = rawData
)
currExp.addLoop(trials)

if prac_repeat == 0:
    currExp.addLoop(practice)

# ---- Start Practice ---
if prac_repeat == 0:
    prac_block = 0
    prac_trial = len(pracMatrix)

    for thisTrial in practice:
        ITI = 1  # Reset ITI to normal

        if prac_trial == len(pracMatrix):
            message = visual.TextStim(
                win,
                text="This is the practice block: " +  "\nPress space to continue",
                color=(-1, -1, -1),  # black
                alignHoriz="center"
            )

            message.draw()
            win.flip()
            event.waitKeys(keyList="spacebar")
            fix.draw()
            win.flip()
            core.wait(2)

        # --- Trial counter ---
        prac_trial = prac_trial - 1
        # --- Set Target Type ---
        targetDetermine = random.random()
        if targetDetermine <= .5:
            target = targ_t
            target_type = 1
        else:
            target = targ_l
            target_type = 2

        # --- Shuffle stim orientation ---
        random.shuffle(orientations)

        # --- Set Cue Location ---
        cue.pos = locations[thisTrial["cueLocation"] - 1]
        cue.ori = thisTrial["cueOrientation"]

        # --- Set Target position and orientation ---
        target.pos = locations[thisTrial["targLocation"] - 1]
        target.ori = orientations[0]

        # --- Set Distractor Location ---
        targetLoc = [thisTrial["targLocation"] - 1]  # target location
        allLoc = [0, 1, 2, 3]  # all possible locations
        targ_multiset = collections.Counter(targetLoc)
        dis_multiset = collections.Counter(allLoc)
        disLoc = list((dis_multiset - targ_multiset).elements())

        # --- Set Distractor position and orientation---
        distractor1.pos = locations[disLoc[0]]
        distractor1.ori = orientations[1]
        distractor2.pos = locations[disLoc[1]]
        distractor2.ori = orientations[2]
        distractor3.pos = locations[disLoc[2]]
        distractor3.ori = orientations[3]

        # --- Set Timer for Trial ---
        timer = core.Clock()
        timer.add(objectTime)

        # -- Objects ---
        while timer.getTime() < 0:
            fix.draw()
            rect1.draw()
            rect2.draw()
            win.flip()

        # --- Cue ---
        timer.add(cueTime)
        while timer.getTime() < 0:
            fix.draw()
            rect1.draw()
            rect2.draw()
            cue.draw()
            win.flip()

        # --- First ISI ---
        timer.add(firstISI)
        while timer.getTime() < 0:
            fix.draw()
            rect1.draw()
            rect2.draw()
            win.flip()

        # --- Search Array ---
        timer.add(searchTime)
        while timer.getTime() < 0:
            fix.draw()
            rect1.draw()
            rect2.draw()
            target.draw()
            distractor1.draw()
            distractor2.draw()
            distractor3.draw()
            win.flip()

        # --- Second ISI  ---
        timer.add(secondISI)
        while timer.getTime() < 0:
            fix.draw()
            rect1.draw()
            rect2.draw()
            win.flip()

        # --- Mask and get response ---
        keys = []
        timer.add(5)
        event.clearEvents()
        while timer.getTime() < 0:
            fix.draw()
            rect1.draw()
            rect2.draw()
            mask1.draw()
            mask2.draw()
            mask3.draw()
            mask4.draw()
            win.flip()

            keys = event.getKeys(keyList=["c", "m", "q"])
            if len(keys) > 0:
                keyDown = keys[0]  # take the first keypress as the response
                timer.reset()
                if keyDown == "q":
                    core.quit()
            elif len(keys) == 0:
                keyDown = None

        # --- Response Check ---
        if target_type == 1 and keyDown == "c":
            corr = 100
        elif target_type == 2 and keyDown == "m":
            corr = 100
        else:
            corr = 0

        # --- Calculate Reward ---
        if thisTrial["reward"] == 1 and corr == 100:
            points = 1
        elif thisTrial["reward"] == 2 and corr == 100:
            points = 6
        elif thisTrial["reward"] == 1 and corr == 0:
            points = -1
        elif thisTrial["reward"] == 2 and corr == 0:
            points = -6


        total = total + points

        # --- Render up Feedback Message ---
        curr_points = str(abs(points))
        total_points = str(total)

        if keyDown != None and corr == 100:
            points_message = "Point(s) earned: " + curr_points + "\nTotal points: " + total_points
            points_message = visual.TextStim(win, text=points_message, colorSpace="rgb", color=[-1, -1, -1],
                                             alignVert="center", pos=[0, 4])
        elif keyDown != None and corr == 0:
            if exp_iter != 4:
                points_message = "Point(s) lost: " + curr_points + "\nTotal points: " + total_points
            elif exp_iter == 4:
                points_message = "Incorrect" + "\nTotal points: " + total_points
            points_message = visual.TextStim(win, text=points_message, colorSpace="rgb", color=[0.3, -1, -1],
                                             alignVert="center", pos=[0, 4])
        elif keyDown == None:
            if exp_iter != 4:
                points_message = "Point(s) lost: " + curr_points + "\nTotal points: " + total_points + "\nPlease Respond Next Trial"
            elif exp_iter == 4:
                points_message = "Incorrect" + "\nTotal points: " + total_points + "\nPlease Respond Next Trial"
            points_message = visual.TextStim(win, text=points_message, colorSpace="rgb", color=[0.3, -1, -1],
                                             alignVert="center", pos=[0, 4])
            ITI = 5

        # --- ITI with Feedback ---
        timer.add(ITI)
        while timer.getTime() < 0:
            fix.draw()
            points_message.draw()
            win.flip()

# --- Set Clocks for Timing ---
fixClock    = core.Clock()  # clock for fixation
objClock    = core.Clock()  # clock for objects
targClock   = core.Clock()  # clock for targets
rtClock     = core.Clock()  # sets up response clock for RT printout
trialClock  = core.Clock()  # sets up time for trial
blockClock  = core.Clock()  # sets up time for run
expClock    = core.Clock()  # clock for whole experiment

# --- Blocks and Current Trial ---
block       = 0
trial       = 0
curTrial    = len(trialMatrix)
total       = 0
disp_acc    = []
disp_rt     = []

# --- Start Experimental Trial Loop ---
for thisTrial in trials:
    rt = rtClock.getTime()
    remBlock = reps - block  # display remaining blocks
    ITI = 1     # Reset ITI to normal

    # --- Display Remaining Blocks ---
    if np.mod(curTrial, blockSize) == 0:
        if trial == 0:
            message = visual.TextStim(
                win,
                text = str(remBlock) + " blocks remaining" + "\n\n\n\n\n\nPress space to continue",
                color = (-1, -1, -1),  # black
                alignHoriz = "center"
            )
        # display RT average, accuracy average, total points after 1st block
        elif trial > 0 :
            avg_points  = total_points
            avg_RT      = round(np.mean(disp_rt)*1000,2)
            avg_acc     = round(np.mean(disp_acc),2)
            message = visual.TextStim(
                win,
                text = str(remBlock) + " blocks remaining" + "\n\nTotal Points: " + avg_points  +"\n\nAccuracy: "+ str(avg_acc) + " %"+ "\n\nAverage RT: "+ str(avg_RT) +" ms" + "\n\nPress space to continue",
                color = (-1, -1, -1),  # black
                alignHoriz = "center"
            )
        message.draw()
        fix.draw()
        win.flip()
        event.waitKeys(keyList="spacebar")

        # --- Recalibration of variables (block #, average RT, etc) ---
        block = block + 1
        disp_rt = []
        disp_acc = []
        fix.draw()
        win.flip()
        core.wait(2)

    # --- Trial counter ---
    curTrial    = curTrial - 1
    trial       = trial + 1

    # --- Set Target Type ---
    targetDetermine = random.random()
    if targetDetermine <= .5:
        target = targ_t
        target_type = 1
    else:
        target = targ_l
        target_type = 2

    # --- Shuffle stim orientation ---
    random.shuffle(orientations)

    # --- Set Cue Location ---
    cue.pos = locations[thisTrial["cueLocation"] - 1]
    cue.ori = thisTrial["cueOrientation"]

    # --- Set Target position and orientation ---
    target.pos = locations[thisTrial["targLocation"]-1]
    target.ori = orientations[0]

    # --- Set Distractor Location ---
    targetLoc       = [thisTrial["targLocation"]-1] # target location
    allLoc          = [0,1,2,3]                     # all possible locations
    targ_multiset   = collections.Counter(targetLoc)
    dis_multiset    = collections.Counter(allLoc)
    disLoc           = list((dis_multiset - targ_multiset).elements())

    # --- Set Distractor position and orientation---
    distractor1.pos = locations[disLoc[0]]
    distractor1.ori = orientations[1]
    distractor2.pos = locations[disLoc[1]]
    distractor2.ori = orientations[2]
    distractor3.pos = locations[disLoc[2]]
    distractor3.ori = orientations[3]

    # --- Set Timer for Trial ---
    timer = core.Clock()
    timer.add(objectTime)

    # -- Objects ---
    while timer.getTime() < 0:
        fix.draw()
        rect1.draw()
        rect2.draw()
        win.flip()

    # --- Cue ---
    timer.add(cueTime)
    while timer.getTime() < 0:
        fix.draw()
        rect1.draw()
        rect2.draw()
        cue.draw()
        win.flip()

    # --- First ISI ---
    timer.add(firstISI)
    while timer.getTime() < 0:
        fix.draw()
        rect1.draw()
        rect2.draw()
        win.flip()

    # --- Search Array ---
    timer.add(searchTime)
    while timer.getTime() < 0:
        fix.draw()
        rect1.draw()
        rect2.draw()
        target.draw()
        distractor1.draw()
        distractor2.draw()
        distractor3.draw()
        win.flip()

    # --- Second ISI  ---
    timer.add(secondISI)
    while timer.getTime() < 0:
        fix.draw()
        rect1.draw()
        rect2.draw()
        win.flip()

    # --- Mask and get response ---
    keys = []
    win.callOnFlip(rtClock.reset)  # this is when RT is being collected
    timer.add(5)
    event.clearEvents()
    while timer.getTime() < 0:
        fix.draw()
        rect1.draw()
        rect2.draw()
        mask1.draw()
        mask2.draw()
        mask3.draw()
        mask4.draw()
        win.flip()

        keys = event.getKeys(keyList=["c", "m", "q"])
        if len(keys) > 0:
            keyDown = keys[0]  # take the first keypress as the response
            rt = rtClock.getTime()
            timer.reset()
            if keyDown == "q":
                core.quit()
        elif len(keys) == 0:
            keyDown = None

    # --- Response Check ---
    if target_type == 1 and keyDown == "c":
        corr = 100
    elif target_type == 2 and keyDown == "m":
        corr = 100
    else:
        corr = 0

    # --- Calculate Reward ---
    if thisTrial["reward"] == 1 and corr == 100:
        points = 1
    elif thisTrial["reward"] == 2 and corr == 100:
        points = 6
    elif thisTrial["reward"] == 1 and corr == 0:
        points = -1
    elif thisTrial["reward"] == 2 and corr == 0:
        points = -6

    total = total + points

    # --- Render up Feedback Message ---
    curr_points     = str(abs(points))
    total_points    = str(total)

    if keyDown != None and corr == 100:
        points_message = "Point(s) earned: " + curr_points + "\nTotal points: " + total_points
        points_message = visual.TextStim(win, text=points_message, colorSpace="rgb", color=[-1, -1, -1], alignVert="center", pos = [0,4])
    elif keyDown != None and corr == 0:
        if exp_iter != 4:
            points_message = "Point(s) lost: " + curr_points + "\nTotal points: " + total_points
        elif exp_iter == 4:
            points_message = "Incorrect" + "\nTotal points: " + total_points
        points_message = visual.TextStim(win, text=points_message, colorSpace="rgb", color=[0.3, -1, -1], alignVert="center", pos = [0,4])
    elif keyDown == None:
        if exp_iter != 4:
            points_message = "Point(s) lost: " + curr_points + "\nTotal points: " + total_points + "\nPlease Respond Next Trial"
        elif exp_iter == 4:
            points_message = "Incorrect" + "\nTotal points: " + total_points + "\nPlease Respond Next Trial"
        points_message = visual.TextStim(win, text=points_message, colorSpace="rgb", color=[0.3, -1, -1], alignVert="center", pos = [0,4])
        ITI = 5

    # --- ITI with Feedback ---
    timer.add(ITI)
    while timer.getTime() < 0:
        fix.draw()
        points_message.draw()
        win.flip()

    # --- Store Response, RT and other Data ---
    trials.addData("roomName",roomName)
    trials.addData("exp_ver", exp_ver)
    trials.addData("exp_iter", exp_iter)
    trials.addData("par_ID", parNo)
    trials.addData("par_age", parAge)
    trials.addData("par_gen", parGen)
    trials.addData("par_hand", parHand)
    trials.addData("orientation", ori)
    trials.addData("blockNo", block)
    trials.addData("trialNo", trial)
    trials.addData("accuracy", corr)
    trials.addData("RT", rt * 1000)
    trials.addData("points_current", curr_points)
    trials.addData("ponts_total", total_points)
    trials.addData("target_type", target_type)
    trials.addData("target orientation",orientations[0])
    trials.addData("dist1_orientation", orientations[1])
    trials.addData("dist2_orientation", orientations[2])
    trials.addData("dist3_orientation", orientations[3])
    currExp.nextEntry()

    # Store RT and accuracy for display
    disp_acc.append(corr)
    disp_rt.append(rt)

message = visual.TextStim(
    win,
    text="You have completed the experiment!" + "\n\nThank you for participating!",
    color=(-1, -1, -1),  # black
    alignHoriz="center"
)
message.draw()
win.flip()
event.waitKeys(keyList="q")

## --- START EMAIL PART --- ##
message2 = visual.TextStim(
    win,
    text="e-mailing data... hang in there",
    color = (-1),
    alignHoriz="center"
)
message2.draw()
win.flip()
if send_email == 1:
    # save files for email
    os.chdir("./data")
    currExp.saveAsWideText(rawName + "_email" + ".csv", delim = ",")

    ## --- EMAIL DATA --- ##
    import smtplib
    from email.MIMEMultipart import MIMEMultipart
    from email.MIMEBase import MIMEBase
    from email.MIMEText import MIMEText
    from email.Utils import COMMASPACE, formatdate
    from email import Encoders

    ## -- email information --
    sender_email    = "nahdata890@gmail.com"
    sender_pass     = "GXe4Ksm6nnZJfo6EfTqRFvm8cMHVsZ"
    mail_server     = "smtp.gmail.com"
    to_email        = "nahdata890@gmail.com"
    end_date        = dt.datetime.now().strftime("%H-%M-%S")
    subject = "Exp: %s %s, par: %s" %("oba-reward", expVer, parNo)
    if paid_exp == 1:
        text = "Participant %s has completed experiment %s %s on %s. Pay " %(parNo, "oba-reward",expVer , expdate)
    else:
        text = "Participant %s has started experiment %s %s on %s and finished on %s." %(parNo, "oba-reward", expVer , date, end_date)

    attachments = [rawName + "_email" + ".csv"]

    def sendMail(sender_email="fake_email@provider.com", sender_pass="fake_password",
                 to=['fake_re...@fake.com'], cc=None, subject="hello wabbit!", text="body text here please!",
                 files=None, SMTP_SERVER="smtp.gmail.com", SMTP_PORT="587"):
        msg = MIMEMultipart()
        msg['From'] = sender_email
        msg['To'] = COMMASPACE.join(to)
        if cc is not None:
            msg['Cc'] = COMMASPACE.join(cc)
        msg['Date'] = formatdate(localtime=True)
        msg['Subject'] = subject

        msg.attach(MIMEText(text))

        if files is not None:
            for file in files:
                part = MIMEBase('application', "octet-stream")
                inputFile = open(file, "rb").read()
                part.set_payload(inputFile)
                Encoders.encode_base64(part)
                part.add_header('Content-Disposition', 'attachment; filename="%s"'
                                % os.path.basename(file))
                msg.attach(part)

        session = smtplib.SMTP(SMTP_SERVER, SMTP_PORT)
        session.ehlo()
        session.starttls()
        session.ehlo
        session.login(sender_email, sender_pass)

        session.sendmail(sender_email, to, msg.as_string())
        session.quit()

    # send e-mail
    sendMail(sender_email=sender_email, sender_pass=sender_pass, to=to_email, subject=subject, text=text, files=attachments)

win.close()
